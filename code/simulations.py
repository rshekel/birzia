import matplotlib.pyplot as plt

from figures import save_sanity_delta, save_sanity_linear, save_analytic_x, save_dud_off, save_dud_thermostat, \
    save_dud_on_no_thermostat
from dud_solver import DudSolver


def heat_sanity_delta():
    dud = DudSolver()
    dud.set_ic_delta()
    # dud.set_ic_side_delta()
    dud.run_heat()
    save_sanity_delta(dud)


def heat_sanity_linear():
    dud = DudSolver()
    dud.set_ic_const_sides()
    dud.run_heat()
    save_sanity_linear(dud)


def dud_off(mul_kappa=1):
    dud = DudSolver()
    dud.const_temp_shower = True
    dud.dud_on = False
    dud.with_thermostat = False
    dud.set_ic_dud(mul_kappa=mul_kappa)
    dud.run_dud()
    # dud.plot_heatmap()
    # dud.plot_T_h()
    save_dud_off(dud, mul_kappa=mul_kappa)


def dud_thermostat():
    dud = DudSolver()
    dud.const_temp_shower = True
    dud.dud_on = True
    dud.with_thermostat = True
    dud.set_ic_dud(mul_kappa=1)
    dud.run_dud()
    save_dud_thermostat(dud)


def dud_on_no_thermostat():
    dud = DudSolver()
    dud.const_temp_shower = True
    dud.dud_on = True
    dud.with_thermostat = False
    dud.set_ic_dud(mul_kappa=1)
    dud.run_dud()
    save_dud_on_no_thermostat(dud)


def heat_dud():
    dud = DudSolver()
    dud.with_thermostat = True
    dud.set_ic_cold()
    dud.run_dud_on_no_shower()
    dud.plot_heatmap()


if __name__ == "__main__":
    # save_analytic_x()
    # heat_sanity_delta()
    # heat_sanity_linear()
    # dud_off(mul_kappa=1)
    # dud_off(mul_kappa=10)
    # heat_dud()
    # dud_on_no_thermostat()
    dud_thermostat()
    plt.show()
