import numpy as np
import matplotlib.pyplot as plt


class DudSolver(object):
    """
    Look these up:
        https://www.uni-muenster.de/imperia/md/content/physik_tp/lectures/ws2016-2017/num_methods_i/heat.pdf
    This ^ has several methods and stability analysis
        http://people.uncw.edu/hermanr/pde1/NumHeatEqn.pdf
    This ^ has actual code example
    """

    def __init__(self):
        # System consts
        # https://www.ideal-ltd.co.il/company-products/electrical-boiler/electric-boilers-specifications/
        self.L = 1  # m (1.072)
        # self.R = 0.57 / 2  # m - This is according to spec., but gives 250 litres.
        self.R = 0.22  # m - this makes pi*R**2 = 150 litres
        self.k = 0.143e-6  # m^2/s https://en.wikipedia.org/wiki/Thermal_diffusivity
        self.power_in = 2500  # Watt - [Q/s]

        self.C = 4184  # [Q / T*kg]

        self.T_h = 70  # c
        self.T_c = 20  # c
        self.T_w = 35  # c

        self.F = 10  # litre / m
        F = self.F / 60  # litre / s
        F = F / 1000  # m^3 / s
        self.F_x = F / (np.pi * self.R ** 2)  # m / s

        # Simulation consts
        self.N_x = 500
        self.X = np.linspace(0, self.L, self.N_x)

        self.T = 60 * 60 * 15  # s
        self.N_t = 30000

        print('alpha:', self.alpha)
        assert self.alpha < 0.5

        # specific simulation
        self.mat = np.zeros((self.N_x, self.N_t))
        # BC is either the constant temperature or -1 for constant derivative
        self.left_BC = -1
        self.right_BC = -1
        self.dud_on = False
        self.const_temp_shower = True
        self.with_thermostat = False

    @property
    def dt(self):
        return self.T / self.N_t

    @property
    def dx(self):
        return self.X[1] - self.X[0]

    @property
    def alpha(self):
        return self.k * self.dt / (self.dx ** 2)

    @property
    def x_angle(self):
        last_pixel = self.mat[-1, :]
        index = np.nonzero(last_pixel)[0][-1]
        curr_Th = last_pixel[index]

        if np.abs(curr_Th - self.T_c) < 0.01:
            return 1
        # print(curr_Th)
        x = (self.T_w - self.T_c) / (curr_Th - self.T_c)
        return min(1, x)

    @property
    def real_F_x(self):
        if self.const_temp_shower:
            return self.F_x * self.x_angle
        else:
            return self.F_x

    def step_heat_eq(self, vec):
        """ return the new vec after time dt. """

        L = vec[0] if self.left_BC == -1 else float(self.left_BC)
        R = vec[-1] if self.right_BC == -1 else float(self.right_BC)

        bigger = np.concatenate(([L], vec, [R]))
        laplacian = np.diff(bigger, n=2)

        new_vec = vec + self.alpha * laplacian

        return new_vec

    def step_stream(self, vec, T_c):
        new = np.concatenate(([T_c], vec[:-1]))
        return new

    def step_dud_on(self, vec):
        if self.with_thermostat:
            last_pixel = self.mat[-1, :]
            index = np.nonzero(last_pixel)[0][-1]
            curr_temps = self.mat[:, index]
            if curr_temps[self.N_x // 3: (self.N_x * 2) // 3].mean() >= self.T_h:
                return vec

        Detla_T_per_kg = (self.power_in * self.dt) / self.C  # [kg * T]
        mass_kg = 1e3 * (np.pi * self.R**2 * self.L)  # [kg] No nedd to multiply by dx - the whole dud heats together,
        # and this is the energy that comes in to it all
        Detla_T = Detla_T_per_kg / mass_kg

        new_vec = vec + Detla_T
        return new_vec

    def run_heat(self):
        for i in range(1, self.N_t):
            self.mat[:, i] = self.step_heat_eq(self.mat[:, i - 1])

    def run_dud(self):
        curr_T = 0
        x_out = 0
        # F = dx/dt
        # dt time passed - so we need to move everything by F*dt
        # once F*dt is dx - move all matrix by one

        for i in range(1, self.N_t):
            self.mat[:, i] = self.step_heat_eq(self.mat[:, i - 1])

            curr_T += self.dt
            x_out += self.dt * self.real_F_x
            if x_out >= self.dx:
                x_out -= self.dx
                print(i)
                self.mat[:, i] = self.step_stream(self.mat[:, i], T_c=self.T_c)

            if self.dud_on:
                self.mat[:, i] = self.step_dud_on(self.mat[:, i])

    def run_dud_on_no_shower(self):
        for i in range(1, self.N_t):
            self.mat[:, i] = self.step_dud_on(self.mat[:, i - 1])

    def plot_heatmap(self):
        fig, ax = plt.subplots()
        ax.imshow(self.mat, cmap='jet', aspect='auto', extent=[0, self.T / 3600, 0, self.L])
        ax.set_xlabel("time (h)")
        ax.set_ylabel("x (m)")
        fig.show()

    def plot_final_temp(self):
        fig, ax = plt.subplots()
        ax.set_title('final distribution')
        ax.plot(self.mat[:, -1])
        fig.show()

    def plot_T_h(self):
        fig, ax = plt.subplots()
        ax.set_title('T_h over time')
        ax.plot(self.mat[-1, :])
        fig.show()

    def set_ic_delta(self):
        vec = np.ones(self.N_x) * 0
        width = 10
        vec[self.N_x // 2 - width: self.N_x // 2 + width] = 100
        self.mat[:, 0] = vec
        self.left_BC = -1
        self.right_BC = -1

    def set_ic_side_delta(self):
        vec = np.ones(self.N_x, dtype='float64') * 0
        width = 10
        st = 50
        vec[st: st + width] = 100
        self.mat[:, 0] = vec
        self.left_BC = -1
        self.right_BC = -1

    def set_ic_const_sides(self):
        # vec = np.ones(self.N_x, dtype='float64') * (self.T_h+self.T_c)/2
        vec0 = (np.ones(self.N_x, dtype='float64') * (self.T_h + self.T_c) / 2)
        vec1 = ((self.T_h - self.T_c) / 2) * np.cos(4 * np.pi * self.X / self.L)
        vec = vec0 + vec1
        self.T *= 12
        self.N_t = 2*self.N_t
        self.mat = np.zeros((self.N_x, self.N_t))
        assert self.alpha < 0.5

        self.mat[:, 0] = vec
        self.left_BC = self.T_h
        self.right_BC = self.T_c

    def set_ic_dud(self, mul_kappa=1):
        self.T /= 12
        self.N_t = int(self.N_t*1.5)
        self.k *= mul_kappa  # because water is pushed in or something...
        self.mat = np.zeros((self.N_x, self.N_t))
        assert self.alpha < 0.5

        vec = np.ones(self.N_x, dtype='float64') * self.T_h
        self.mat[:, 0] = vec
        self.left_BC = self.T_c
        # self.left_BC = -1
        self.right_BC = -1

    def set_ic_cold(self):
        self.T /= 3
        self.N_t //= 3
        self.mat = np.zeros((self.N_x, self.N_t))
        assert self.alpha < 0.5

        vec = np.ones(self.N_x, dtype='float64') * self.T_c
        self.mat[:, 0] = vec
        self.right_BC = -1
        self.left_BC = -1
