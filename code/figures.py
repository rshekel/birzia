import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit


PATH_ANALYTIC_X = "../misc/analytic_x.png"
PATH_HEAT_SANITY_DELTA = "../misc/heat_sanity_delta.png"
PATH_HEAT_SANITY_LINEAR = "../misc/heat_sanity_linear.png"
PATH_DUD_ON_THERMOSTAT = "../misc/dud_on_thermostat.png"
PATH_DUD_ON_NO_THERMOSTAT = "../misc/dud_on_no_thermostat.png"

# open(PATH_ANALYTIC_X, 'w').write('a')  # sanity check for relative paths


def gaus(x, x0, A, C, sigma):
    return A * np.exp(-(x - x0) ** 2 / (2 * sigma ** 2)) + C


def linear(x, a, b):
    return a*x + b


def save_analytic_x():
    T_h0 = 70
    T_c = 20
    T_w = 40
    F = (10e-3 / 60)  # 10 litre/min - Chascham gives 8-11
    V = np.pi * 0.22**2  # 150 litres

    t_c = (V/F) * (T_h0-T_w) / (T_w-T_c)

    t = np.linspace(0, 1.1*t_c, 10000)
    x = (T_w-T_c) / (T_h0 - T_c - (F/V)*(T_w-T_c)*t)

    fig, ax = plt.subplots()
    ax.plot(t/60, x)
    ax.axvline(t_c/60, linestyle='--', color='g')
    ax.set_xlabel("time (m)")
    ax.set_ylabel("faucet angle / $\pi$")
    fig.show()
    fig.savefig(PATH_ANALYTIC_X)


def save_sanity_delta(dud):
    plt.rcParams.update({'font.size': 12})
    plt.rcParams["figure.figsize"] = (9, 4)

    fig, axes = plt.subplots(1, 2)
    im = axes[0].imshow(dud.mat, cmap='jet', aspect='auto', extent=[0, dud.T / 3600, 0, dud.L])
    axes[0].set_xlabel("time (h)")
    axes[0].set_ylabel("x (m)")
    fig.colorbar(im, ax=axes[0])

    axes[1].plot(dud.X, dud.mat[:, -1], label='Simulated temperature')
    axes[1].set_xlabel("x (m)")
    axes[1].set_ylabel("tempereatrue (C)")

    vals, pcov = curve_fit(gaus, dud.X, dud.mat[:, -1])
    axes[1].plot(dud.X, gaus(dud.X, *vals), '--', label='Fit to Gaussian')

    axes[1].legend(loc=0, prop={'size': 8})

    fig.savefig(PATH_HEAT_SANITY_DELTA)


def save_sanity_linear(dud):
    plt.rcParams.update({'font.size': 12})
    plt.rcParams["figure.figsize"] = (9, 4)

    fig, axes = plt.subplots(1, 2)
    im = axes[0].imshow(dud.mat, cmap='jet', aspect='auto', extent=[0, dud.T / 3600, 0, dud.L])
    axes[0].set_xlabel("time (h)")
    axes[0].set_ylabel("x (m)")
    fig.colorbar(im, ax=axes[0])

    axes[1].plot(dud.X, dud.mat[:, -1], label='Simulated temperature')
    axes[1].set_xlabel("x (m)")
    axes[1].set_ylabel("tempereatrue (C)")

    vals, pcov = curve_fit(linear, dud.X, dud.mat[:, -1])
    axes[1].plot(dud.X, linear(dud.X, *vals), '--', label='Fit to linear')

    axes[1].legend(loc=0, prop={'size': 8})


    fig.savefig(PATH_HEAT_SANITY_LINEAR)


def save_dud_off(dud, mul_kappa=1):
    PATH_DUD_OFF = f"../misc/dud_off_mul_kappa={mul_kappa}.png"
    plt.rcParams.update({'font.size': 12})
    plt.rcParams["figure.figsize"] = (9, 4)

    fig, axes = plt.subplots(1, 2)
    im = axes[0].imshow(dud.mat, cmap='jet', aspect='auto', extent=[0, dud.T / 60, 0, dud.L])
    axes[0].set_xlabel("time (m)")
    axes[0].set_ylabel("x (m)")
    fig.colorbar(im, ax=axes[0])

    t = np.linspace(0, dud.T, dud.N_t)

    axes[1].plot(t/60, dud.mat[-1, :])
    axes[1].set_xlabel("t (m)")
    axes[1].set_ylabel("Faucet tempereatrue (C)")

    fig.savefig(PATH_DUD_OFF)


def save_dud_thermostat(dud):
    plt.rcParams.update({'font.size': 12})
    plt.rcParams["figure.figsize"] = (9, 4)

    fig, axes = plt.subplots(1, 2)
    im = axes[0].imshow(dud.mat, cmap='jet', aspect='auto', extent=[0, dud.T / 60, 0, dud.L])
    axes[0].set_xlabel("time (m)")
    axes[0].set_ylabel("x (m)")
    fig.colorbar(im, ax=axes[0])

    t = np.linspace(0, dud.T, dud.N_t)

    axes[1].plot(t / 60, dud.mat[-1, :])
    axes[1].set_xlabel("t (m)")
    axes[1].set_ylabel("Faucet tempereatrue (C)")

    fig.savefig(PATH_DUD_ON_THERMOSTAT)


def save_dud_on_no_thermostat(dud):
    plt.rcParams.update({'font.size': 12})
    plt.rcParams["figure.figsize"] = (9, 4)

    fig, axes = plt.subplots(1, 2)
    im = axes[0].imshow(dud.mat, cmap='jet', aspect='auto', extent=[0, dud.T / 60, 0, dud.L])
    axes[0].set_xlabel("time (m)")
    axes[0].set_ylabel("x (m)")
    fig.colorbar(im, ax=axes[0])

    t = np.linspace(0, dud.T, dud.N_t)

    axes[1].plot(t / 60, dud.mat[-1, :])
    axes[1].set_xlabel("t (m)")
    axes[1].set_ylabel("Faucet tempereatrue (C)")

    fig.savefig(PATH_DUD_ON_NO_THERMOSTAT)
